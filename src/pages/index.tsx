import React from 'react';
import Img from 'gatsby-image';
import { SEO } from '../components/seo';
import { Layout } from '../components/Layout';
import { graphql, useStaticQuery } from 'gatsby';
import recordImage from '../images/svg/record.svg';
import * as style from './index.module.scss';

const HomePage = () => {
  const data = useStaticQuery(graphql`
    query {
      mainImage: file(relativePath: { eq: "watersmoke.png" }) {
        childImageSharp {
          fluid(maxHeight: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `);

  return (
    <>
      <Layout>
        <SEO props={{ title: 'Home', meta: [] }} />
        <div className={style.content}>
          <div className={style.welcomeSection}>
            <div className={style.backgroundContainer} />
            <div className={style.imageContainer}>
              <Img className={style.mainImage} fluid={data.mainImage.childImageSharp.fluid} />
            </div>
            <div className={style.foregroundContainer}>
              <div className={style.joinus}>
                <span>We are not just a music room - we are a community.</span>
                <a href="https://app.jqbx.fm/room/cbl"><span>Join us on JQBX</span></a>
              </div>
              <div className={style.title}>
                <span>CHILL</span>
                <span>BUT</span>
                <span>LIT.</span>
              </div>
            </div>
          </div>
          <div id="about" className={style.aboutSection}>
            <div className={style.sectionContainer}>
              <h2>We are Chill but Lit.</h2>
              <p>
                We are a JQBX community centered around electronic music in every form and shape,
                with focus on tracks that send your mind on a chill adventure while still packing a punch.
                Regardless of wether you are into minimal techno style of Kollektiv Turmstraße
                or prefer the organic sounds of Plantrae, you will definitely find your place here.
              </p>
            </div>
            <div className={style.sectionContainer}>
              <h2>We are passionate.</h2>
              <p>
                All our members are very passionate about music. We love discovering new tracks, comparing remixes
                and experimenting with rare genres. They say the sky is the limit, but we already prepared our spaceship.
              </p>
            </div>
            <div className={style.sectionContainer}>
              <h2>You are welcome.</h2>
              <p>
                No matter who you are, You are welcome to join us and become a part of the CBL family.
                We love seeing new faces and hearing different musical angles You have to present.
                Many newcomers quickly become room regulars, and we hope You will too!
              </p>
            </div>
          </div>
          <div id="rules" className={style.rulesSection}>
            <h2>Rules</h2>
            <div className={style.sectionContainer}>
              <h3>Be respectful.</h3>
              <p>
                We do not allow downvoting, as we believe it's not productive. If there is something about the play you don't like,
                please discuss it in chat. Be excellent to each other, and help us maintain the chill atmosphere we have cultivated.
              </p>
            </div>
            <div className={style.sectionContainer}>
              <h3>Avoid most lyrics.</h3>
              <p>
                While vocals that take the part of an instrument (vox) are alright, we prefer to say away from lyrics-focused tracks.
                If you would like to play something that has a greater focus on singing - ask other room members. Many times, people
                will be okay with it as long as you asked.
              </p>
            </div>
            <div className={style.sectionContainer}>
              <h3>Follow the flow.</h3>
              <p>
                As a rule of thumb, try to follow the current vibe of the room. Do you notice a certain instrument, tempo or theme in
                the current song? See if you can follow it with a fitting play. While this is not a strict requirement, it does help
                curating the environment and enchances the social aspect of DJing.
              </p>
            </div>
            <div className={style.sectionContainer}>
              <h3>Skipping and removing DJs.</h3>
              <p>
                If you go AFK or play something that does not fit the vibe of the room / violates the rules, you may be skipped.
                Not to worry, though - it's not personal and you may reclaim your spot when you have something that fits within
                the room context.
              </p>
            </div>
            <div className={style.sectionContainer}>
              <h3>Still have questions?</h3>
              <p>
                Feel free to ask the room staff, as we are happy to help answer any questions you have.
              </p>
            </div>
            <img src={recordImage} />
          </div>
        </div>
      </Layout>
    </>
  )
};

export default HomePage;
