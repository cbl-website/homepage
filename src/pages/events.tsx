import React, { useState, useRef, useEffect } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { LayoutNoFooter } from '../components/LayoutNoFooter';
import { parseISO, format, isValid, isBefore, isAfter, addDays, setHours, setMinutes, isEqual } from 'date-fns';
import { useSnackbar } from 'notistack';
import * as style from './events.module.scss';

import './calendar.scss';
import { subDays, addHours } from 'date-fns/esm';

const EventsPage = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');

  const calendar = useRef<FullCalendar>(null);
  const startDateInput = useRef<HTMLInputElement>(null);
  const endDateInput = useRef<HTMLInputElement>(null);

  const setDateRange = (data: { start: Date, end: Date }) => {
    setStartDate(data.start);
    setEndDate(subDays(data.end, 1));
  }

  const handleStartDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const nextStartDate = parseISO(event.target.value);
    if (isValid(nextStartDate)) {
      setStartDate(parseISO(event.target.value));
    }
  }

  const handleEndDateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const nextEndDate = parseISO(event.target.value);
    if (isValid(nextEndDate)) {
      setEndDate(parseISO(event.target.value));
    }
  }

  const handlestartDateBlur = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (isAfter(startDate, endDate)) {
      enqueueSnackbar('Start date has to occur before end date.');
      calendar.current?.getApi().select(endDate, addDays(endDate, 1));
    } else {
      calendar.current?.getApi().select(startDate, addDays(endDate, 1));
    }
  }

  const handleEndDateBlur = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (isBefore(endDate, startDate)) {
      enqueueSnackbar('Start date has to occur before end date.');
      calendar.current?.getApi().select(endDate, addDays(endDate, 1));
    } else {
      calendar.current?.getApi().select(startDate, addDays(endDate, 1));
    }
  }

  const handleStartTimeBlur = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.value !== '') {
      setStartTime(event.target.value);
    }
  }

  const handleEndTimeBlur = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.value !== '') {
      setEndTime(event.target.value);
    }
  }

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  }

  return (
    <>
      <LayoutNoFooter>
        <div className={style.content}>
          <div className={style.sidebar}>
            <h1>Schedule an event</h1>
            <p>
              If you would like to schedule an event with our room,
              wether it's a reserved DJ set or a special occasion,
              you can do so using the form below. All fields are mandatory.
            </p>
            <form onSubmit={(e) => handleSubmit(e)}>
              <div className={style.verticalGroup}>
                <span>JQBX Username</span>
                <input spellCheck="false" type="text" />
              </div>

              <div className={style.verticalGroup}>
                <span>Email address</span>
                <input spellCheck="false" type="text" />
              </div>

              <div className={style.verticalGroup}>
                <span>Name of the event</span>
                <input spellCheck="false" type="text" />
              </div>

              <div className={style.verticalGroup}>
                <span>Event description</span>
                <input spellCheck="false" type="text" />
              </div>

              <div className={style.horizontalGroup}>
                <div className={style.verticalGroup}>
                  <span>Start date</span>
                  <input
                    value={format(startDate, 'yyyy-MM-dd')}
                    ref={startDateInput}
                    spellCheck="false"
                    type="date"
                    onChange={(e) => handleStartDateChange(e)}
                    onBlur={(e) => handlestartDateBlur(e)}
                  />
                </div>

                <div className={style.verticalGroup}>
                  <span>Start time</span>
                  <input
                    spellCheck="false"
                    type="time"
                    onBlur={(e) => handleStartTimeBlur(e)}
                  />
                </div>
              </div>
              <div className={style.horizontalGroup}>
                <div className={style.verticalGroup}>
                  <span>End date</span>
                  <input
                    value={format(endDate, 'yyyy-MM-dd')}
                    ref={endDateInput}
                    spellCheck="false"
                    type="date"
                    onChange={(e) => handleEndDateChange(e)}
                    onBlur={(e) => handleEndDateBlur(e)}
                  />
                </div>

                <div className={style.verticalGroup}>
                  <span>End time</span>
                  <input
                    spellCheck="false"
                    type="time"
                    onBlur={(e) => handleEndTimeBlur(e)}
                  />
                </div>
              </div>
              <input type="submit" value="Submit event request!" />
            </form>
          </div>
          <div className={style.calendarContainer}>
            <FullCalendar
              ref={calendar}
              selectable={true}
              unselectAuto={false}
              defaultView="dayGridMonth"
              plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
              header={{
                left: 'dayGridMonth',
                right: 'prevYear,prev,next,nextYear',
              }}
              select={(data) => setDateRange(data)}
              selectConstraint={{
                start: new Date(),
              }}
            />
          </div>
        </div>
      </LayoutNoFooter>
    </>
  );
}

export default EventsPage;
