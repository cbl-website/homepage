import React from 'react';
import { Link } from 'gatsby';
import snowEmoji from '../../images/svg/snow.svg';
import peachEmoji from '../../images/svg/peach.svg';
import fireEmoji from '../../images/svg/fire.svg';
import * as style from './header.module.css';

const navigateToElement = (elementName: string, offset?: number) => {
  const element = document.getElementById(elementName);
  if (!element) {
    return;
  }

  window.scrollTo({ top: element.offsetTop - (offset || 0), behavior: 'smooth' });
}

export const Header = () => (
  <header className={style.header}>
    <div className={style.background} />
    <div className={style.foreground}>
      <Link className={style.logo} to="/">
        <img src={snowEmoji} />
        <img src={peachEmoji} />
        <img src={fireEmoji} />
      </Link>
      <nav>
        {
          window.location.pathname === '/'
            ? <>
              <a onClick={() => navigateToElement('about', 220)}>About</a>
              <a onClick={() => navigateToElement('rules')}>Rules</a>
            </> : <>
              <Link to="/#about">About</Link>
              <Link to="/#rules">Rules</Link>
            </>
        }
        <Link to="/events">Events</Link>
        <Link to="/music">Music</Link>
      </nav>
    </div>
  </header>
);
