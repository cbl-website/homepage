import React from 'react';
import { Link } from 'gatsby';
import snowEmoji from '../../images/svg/snow.svg';
import peachEmoji from '../../images/svg/peach.svg';
import fireEmoji from '../../images/svg/fire.svg';
import songEmoji from '../../images/svg/song.svg';
import * as style from './footer.module.css';

export const Footer = () => (
  <footer className={style.footer}>
    <div className={style.column}>
      <h3>Information</h3>
      <Link to="/">Home</Link>
      <Link to="/">About</Link>
      <Link to="/">Rules</Link>
      <Link to="/">Events</Link>
      <Link to="/">Music</Link>
    </div>
    <div className={style.column}>
      <h3>Social</h3>
      <Link to="/">JQBX</Link>
      <Link to="/">Reddit</Link>
      <Link to="/">Discord</Link>
      <Link to="/">Last.fm</Link>
    </div>
    <div className={style.column}>
      <div className={style.logoContainer}>
        <Link className={style.logo} to="/">
          <img src={snowEmoji} />
          <img src={peachEmoji} />
          <img src={fireEmoji} />
        </Link>
        <span>Chill but Lit Ⓒ {new Date().getFullYear()}</span>
      </div>
      <div className={style.subtext}>
        <span>Created with</span>
        <img src={songEmoji} />
        <span>by Kamil Solecki</span>
      </div>
    </div>
  </footer>
);
