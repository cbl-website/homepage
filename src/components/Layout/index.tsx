import React from 'react';
import * as style from './layout.module.css';
import { Header } from '../Header';
import { Footer } from '../Footer';


export const Layout: React.FC = ({ children }) => {
  return (
    <div className={style.container}>
      <Header />
      {children}
      <Footer />
    </div>
  )
};
