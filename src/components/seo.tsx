import React from 'react';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';

export interface SEOProps {
  description?: string;
  meta: Array<React.DetailedHTMLProps<React.MetaHTMLAttributes<HTMLMetaElement>, HTMLMetaElement>>;
  lang?: string;
  title?: string;
}

const defaultSEOProps: SEOProps = {
  lang: 'en',
  meta: [],
  description: '',
}

export const SEO: React.FC<{ props: SEOProps }> = ({ props = defaultSEOProps }) => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `,
  );

  const metaDescription = props.description || site.siteMetadata.description;

  return (
    <Helmet
      htmlAttributes={{ lang: props.lang }}
      title={props.title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[{
        name: `description`,
        content: metaDescription,
      }, {
        property: `og:title`,
        content: props.title,
      }, {
        property: `og:description`,
        content: metaDescription,
      }, {
        property: `og:type`,
        content: `website`,
      }, {
        name: `twitter:card`,
        content: `summary`,
      }, {
        name: `twitter:creator`,
        content: site.siteMetadata.author,
      }, {
        name: `twitter:title`,
        content: props.title,
      }, {
        name: `twitter:description`,
        content: metaDescription,
        },
        ...props.meta,
      ]}
    />
  )
}
