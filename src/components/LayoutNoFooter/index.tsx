import React from 'react';
import * as style from './layoutnofooter.module.css';
import { Header } from '../Header';

export const LayoutNoFooter: React.FC = ({ children }) => {
  return (
    <div className={style.container}>
      <Header />
      {children}
    </div>
  )
};
