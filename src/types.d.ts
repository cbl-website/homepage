declare module '*.css' {
  const styles: any;
  export = styles;
}

declare module '*.scss' {
  const styles: any;
  export = styles;
}

declare module '*.svg' {
  const url: string;
  export default url;
  export const ReactComponent: any;
}
